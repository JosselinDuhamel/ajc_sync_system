Nom de projet : ajc_syns_system
Version Python : 3.9
Version Pip: 3

-------------------------------

Développeurs :

Tiphaine Richard
Joseph Rascar
Julien Guillou
Josselin Duhamel

-------------------------------

Commandes :

Lancement du programme : python 3.9 main.py [DIR 1] [DIR 2]	||	Créé les DIR si ils n'existent pas

HELP				->	Affiche toutes les commandes possibles
INFO				->	Affiche toutes les infos du sync system
STOP				->	Quitte le programme
PAUSE				->	Met en pause la synchronisation
SYNC				->	Relance la synchronisation
CHANGE [DIR 1] [DIR 2]		->	Change les directory à synchroniser
COMPARE [DIR 1] [DIR 2]		->	Compare la taille et date de modification des 2 dossiers en paramètres, ainsi que leurs fichiers et sous dossiers
FILTER [.extension]		->	Créé un filtre d'extensions, à partir de cette commande, met à jour uniquement les fichiers avec l'extension passée en paramètre
ENDFILTER			->	Supprime le filtre, qu'il soit existant ou non

-------------------------------

Le programme peut synchroniser les fichiers ajoutés ou supprimés, ainsi que les dossiers ajoutés ou supprimés