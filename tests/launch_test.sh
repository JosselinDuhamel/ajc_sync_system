echo "---------------------- TEST 1 ------------------------"
echo "---------------- Commandes basiques ------------------"
echo

############ TEST 1 ############

python3.9 ../run.py < test1/test1-joss > result
DIFF=$(diff result test1/result-test1)

if [ "$DIFF" == "" ] 
then
    echo "[TEST 1] SUCCESS"
else
    echo "[TEST 1] FAILED"
    echo $DIFF
fi

rm result
rmdir Folder1
rmdir Folder2
rm file.db
############ END TEST 1 ############
