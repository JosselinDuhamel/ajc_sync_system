#!usr/bin/python3.9
# coding: utf-8

import sys, os
import app
from app.bcolor import bcolors
from app.thread import thread_dir, create_thread
from app.filter import set_filter
from app.endfilter import endfilter
from app.sync import sync
from app.thread import thread_dir
from app.info import info
from app.pause import pause
from app.help import help

'''
Vérifie si les arguments sont des directories, sinon ils les créés.
'''
def set_dir(grid_command, print_opt=""):
    if not os.path.isdir(grid_command[1]):
        os.mkdir(grid_command[1])
    app.dir1 = grid_command[1]
    app.dir2 = grid_command[2]
    print(bcolors.OKBLUE + print_opt + "Directory 1 set to :", app.dir1 + bcolors.ENDC)
    print(bcolors.OKBLUE + "Directory 2 set to :", app.dir2 + bcolors.ENDC + "\n")

'''    
Boucle sur l'input tant qu'il est différent de "STOP", Parse l'input et exécute en fonction de la commande
'''
def start(grid_command, sync_flag, filter_flag):
    while (grid_command[0] != "STOP" and app.stop_flag == False):
        if (grid_command[0] == "HELP"):
            help()
        elif (grid_command[0] == "PAUSE"):
            app.sync_flag = pause()
        elif (grid_command[0] == "SYNC"):
            app.sync_flag = sync()
        elif (grid_command[0] == "FILTER"):
            app.filter_flag = set_filter(grid_command)
        elif (grid_command[0] == "ENDFILTER"):
            app.filter_flag = endfilter()
        elif (grid_command[0] == "INFO"):
            info(app.dir1, app.dir2, app.sync_flag, app.filter_flag)
        elif (grid_command[0] == "CHANGE"):
            set_dir(grid_command, "\n")
        else:
            print("\nUnknown command\n")
        grid_command = input(">> ").split(' ')
    app.stop_flag = True
    return (0)

'''
Départ du programme, affiche le message de bienvenue 
et print un warning si les 2 directories ne sont pas en paramètres
'''
if __name__ == "__main__":
    print("Welcome to our Sync System :\n\nType \"HELP\" for more informations.\n")
    if len(sys.argv) == 3:
        set_dir(sys.argv)
    else:
        print (bcolors.WARNING + "Warning : No directories selected\n" + bcolors.ENDC)
    thread = create_thread()
    thread.start()
    start(input(">> ").split(' '), app.sync_flag, app.filter_flag)
    thread.join()
