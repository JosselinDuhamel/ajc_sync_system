#importation des modules pour tkinter
from tkinter import * 
from tkinter import messagebox, filedialog
import os

#importations modules contenant les fonctions pythons
#pour la manipulation des dossiers

from app.filter import set_filter
from app.sync import sync 
from app.help import help
from app.pause import pause
from app.endfilter import endfilter
from app.info import info

#fonctions
def dossier_manquant():
    messagebox.showerror("ERREUR", "Veuillez choisir deux dossiers à synchroniser")

def meme_fichier_appele():
    messagebox.showerror("ERREUR", "Veuillez choisir deux dossiers différents à synchroniser")

def GetDossier1():
    global dossier1
    dossier1 = filedialog.askdirectory(initialdir="/",title='Choisissez un repertoire')
    if len(dossier1) > 0:
        text_file1.set(dossier1)

def GetDossier2():
    global dossier2
    dossier2 = filedialog.askdirectory(initialdir="/",title='Choisissez un repertoire')
    if len(dossier2) > 0:
        text_file2.set(dossier2)

def verification_chemin():
    return 0
 

#creer une premiere fenêtre
window = Tk()

#personnaliser cette fenêtre
window.title("Synchronisation des Dossiers")

# pour ne pas redimensionner la fenêtre
window.resizable(width=False, height=False)

#----------- A METTRE APRES QUE TOUT SOIT POSITIONNE------
# configuration de la couleur de fond
window.config(background="#41B77F")


label_cote_droit_titre=Label(window, width="25", bg="red")
label_cote_droit_titre_1=Label(window, width="25", bg="red")
label_cote_gauche_titre=Label(window, width="25", bg="green")
label_cote_gauche_titre_1=Label(window, width="25", bg="green")
label_titre=Label(window, text="Gestionnaire de synchronisation des dossiers", font=("Arial", 16), pady=10, bg='#41B77F', fg='white', relief=GROOVE)

label_cote_droit_titre.grid(row=0,column=0)
label_cote_droit_titre_1.grid(row=0,column=1)
label_titre.grid(row=0,column=2)
label_cote_gauche_titre.grid(row=0,column=3)
label_cote_gauche_titre_1.grid(row=0,column=4)

#Textes présentation Fenêtre Tkinter
label_subtitle = Label(window, text="Sélectionnez deux dossiers pour pouvoir lancer la synchronisation", font=("Arial", 12), pady=(10), bg='#41B77F', fg='white' )
label_subtitle.grid(row=1, column=2)
 

## TEST AVEC DES WIDGETS MESSAGE

text_file1 = StringVar()
text_file1.set("Chemin du premier Dossier")

text_file2 = StringVar()
text_file2.set("Chemin du second Dossier")

dossier1_getpath = Label(textvariable=text_file1, bg="#FFF", pady=10, relief=RIDGE)
dossier2_getpath = Label(textvariable=text_file2, bg="#FFF", pady=10, relief=RIDGE)

dossier1_getpath.grid(row=2, column=1, pady=20)
dossier2_getpath.grid(row=2, column=3, pady=20)
#""""
 
dossier1_getfile = Button(window, text="Choisir le premier Dossier", font=("Arial", 10), bg='#41B77F', fg='white',  command=GetDossier1)
dossier1_getfile.grid(row=3, column=1)
dossier2_getfile = Button(window, text="Choisir le second Dossier", font=("Arial", 10), bg='#41B77F', fg='white', command=GetDossier2)
dossier2_getfile.grid(row=3, column=3)



# Liste boutons choix fonctions
Pause_button = Button(window, text="Pause", font=("Arial", 10), bg='#FFF', fg='#41B77F')
Sync_button = Button(window, text="Synchronisation", font=("Arial", 10), bg='#FFF', fg='#41B77F')

#Faire les grids des boutons
Pause_button.grid(row=6, column=1, pady=20)
Sync_button.grid(row=6, column=3, pady=20)




# test_button = Button(window, text="déclencher une erreur", command=fonction_test_input) # pour appeler la fonction avec 
# command fonctionne ausi avec les fonctions des autres modules appelés
# test_button.grid(row = 4, column=0)



# afficher
window.mainloop()
