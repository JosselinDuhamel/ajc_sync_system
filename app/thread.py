#!usr/bin/python3.9
# coding: utf-8

import threading
import app
import sqlite3
from shutil import copytree, rmtree
from time import sleep
from app.dirs_tools import set_len_dirs, len_dir, folder_diff
from app.sync import sync
from app.action import check_type, action
from app.db import check_db, stop_db
from app.myconnect import MyConnect

mycon = MyConnect("file.db")
mycon.connectDB()
cur = mycon.cur
mycon.activate()


'''
Fonction du thread qui va analyser les comportement des deux dossiers
et réagir en fonction des flags
'''
def create_thread():
    return threading.Thread(target=thread_dir, daemon=True)

def thread_dir():
    while (app.dir1 == None and app.dir2 == None and app.stop_flag == False):
        sleep(0.5)
    rmtree(app.dir2, ignore_errors=True)
    copytree(app.dir1, app.dir2, dirs_exist_ok=True)
    set_len_dirs()
    while app.stop_flag == False:
        diff = folder_diff(app.dir1, app.dir2)
        if diff:
            diff = sorted(sorted(diff, key=lambda x: x[-1]), reverse=True)
            action_flag = action()
            check_type(diff, action_flag, mycon, cur)
            sleep(2)
        else:
            sleep(0.5)
    stop_db(mycon)
