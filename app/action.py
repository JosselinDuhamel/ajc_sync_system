#!usr/bin/python3.9
# coding: utf-8

import app
from app.dirs_tools import len_dir, set_len_dirs
from app.file_action import file_action
from app.folder_action import folder_action
from app.db import isExist, save

def check_type(diff, action, mycon, cur):
    action_save = action.split(' ')
    done = False
    for item in diff:
        if app.sync_flag == "Pause":
            if not isExist(cur, action_save[2] + item, action_save[1], action_save[0]):
                if item[len(item) - 1] == '/':
                    save(action_save[2] + item, action_save, mycon, 'dir')
                else:
                    save(action_save[2] + item, action_save, mycon, 'file')
                print('\n>> ', end='')        
        elif item[len(item) - 1] == '/':
            done = folder_action(item, action.split(' '))
        else:
            done = file_action(item, action.split(' '))
    if done == True:
        set_len_dirs()
        print('\n>> ', end='')

def action():
    if len_dir(app.dir1) != app.len_dir1:
        if len_dir(app.dir1) < app.len_dir1:
            return "removed " + app.dir2 + " " + app.dir1 
        else:
            return "created " + app.dir2 + " " + app.dir1
    elif len_dir(app.dir2) != app.len_dir2:
        if len_dir(app.dir2) < app.len_dir2:
            return "removed " + app.dir1 + " " + app.dir2
        else:
            return "created " + app.dir1 + " " + app.dir2
