#!usr/bin/python3.9
# coding: utf-8

def info(dir1, dir2, sync_flag, filter_flag):
    print("\nSync System Info :\n\nDirectory 1 :", str(dir1) + "\nDirectory 2 :", str(dir2) + "\nFilter :", str(filter_flag) +
          "\nSync Status :", sync_flag + "\n")
