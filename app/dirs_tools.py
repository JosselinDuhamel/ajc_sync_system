#!usr/bin/python3.9
# coding: utf-8

import os
import app

'''
Fonction qui retourne le nombre d'items dans 1 directory
'''
def len_dir(dir):
    return sum([len(files + d) for r, d, files in os.walk(dir)])

'''
Fonction qui update le nombre d'items dans les 2 directories
'''
def set_len_dirs():
    app.len_dir1 = sum([len(files + d) for r, d, files in os.walk(app.dir1)])
    app.len_dir2 = sum([len(files + d) for r, d, files in os.walk(app.dir2)])

def ls(path):
    all = []
    for base, sub_f, files in os.walk(path):
        for sub in sub_f:
            entry = os.path.join(base,sub) + '/'
            entry = entry[len(path):].strip("\\")
            all.append(entry)
        for file in files:
            entry = os.path.join(base,file)
            entry = entry[len(path):].strip("\\")
            all.append(entry)
    all.sort()
    return all

def folder_diff(folder1_path, folder2_path):
    folder1_list = ls(folder1_path);
    folder2_list = ls(folder2_path);
    diff = [item for item in folder1_list if item not in folder2_list]
    diff.extend([item for item in folder2_list if item not in folder1_list])
    return (diff)
