#!usr/bin/python3.9
# coding: utf-8

def help():
    print("Command Menu :\n\nHELP\t\t\t->\tDisplay all commands\nINFO\t\t\t->\t" +
          "Display all sync system informations\nSTOP\t\t\t->\tExit Sync System\n" +
          "PAUSE\t\t\t->\tStop syncronization\nSYNC\t\t\t->\tStart syncronization\n" +
          "CHANGE [DIR1] [DIR2]\t->\tUpdate the directories to synchronize\n" +
          "COMPARE [DIR1 [DIR2]\t->\tCompare the size and modification date of" +
          " the 2 directories, as well as their files and subfolders\n" +
          "FILTER [.extension]\t->\tCreate an extensions filter. From this command, " + 
          "update only files with the extension passed in parameter\n" +
          "ENDFILTER\t\t->\tRemove filter\n")
