#!usr/bin/python3.9
# coding: utf-8

from app.bcolor import bcolors

def pause():
    print("\n" + bcolors.WARNING +"Pause sync\n" + bcolors.ENDC)
    return "Pause"
