#!usr/bin/python3.9
# coding: utf-8
from app.bcolor import bcolors

def set_filter(extension):
    if len(extension) == 2:
        print("\n" + bcolors.OKBLUE +"Filter set to the extension : " + bcolors.ENDC + extension[1] + '\n')
        return extension[1]
    else:
        print("\n" + bcolors.FAIL + "Filter failed.\nUSAGE : Filter [.extension]\n" + bcolors.ENDC + "\n")
