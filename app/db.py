import os
import time

def save(path, command, mycon, typef):
    dest = command[1]
    action = command[0]
    if action == "removed":
        size = None
        modif = None
    else:
        size = str(os.path.getsize(path))
        modif = str(time.ctime(os.path.getmtime(path)))
    val = (path, dest, typef, size, action, modif)
    mycon.insertVal(val)
    print ("\n\n[DB MESSAGE]", "Action on", typef , path, "has been recorded in the database")
    
def stop_db(mycon):
    mycon.deactivate()
    mycon.deconnectBD()
    
def check_db(cur):
    cur.execute("SELECT * FROM save;")
    print(cur.fetchall())

def isExist(cur, path, dest, action):
    cur.execute("SELECT * FROM save WHERE path='" + path + "' AND dest='" + dest + "' AND action='" + action + "';")
    # cur.execute('''SELECT * from save WHERE path=path AND dest=dest AND action=action;''')    
    nb = len(cur.fetchall())
    if nb == 0:
        return False
    return True
