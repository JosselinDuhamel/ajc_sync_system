#!usr/bin/python3.9
# coding: utf-8

from shutil import copytree, rmtree
from app.bcolor import bcolors

def find_folder(folder, search_path):
    for dirpath, sub_dirs, filenames in os.walk(search_path):
        if folder in sub_dirs:
            return True
    return False

def sync_folder(folder, action, command, color):
    try:
        exec(command)
        print ("\n\n" +bcolors.OKBLUE + "Sub-directory " + bcolors.ENDC + folder + color,
               action[0], bcolors.OKBLUE + "in both parent directories" +  bcolors.ENDC)
        return True
    except OSError as e:
        print("Error: %s : %s" % (action[1] + folder, e.strerror))
    return False

def folder_action(folder, action):
    if action[0] == "removed":
        return sync_folder(folder, action, "rmtree(action[1] + folder)", bcolors.FAIL)
    if action[0] == "created":
        return sync_folder(folder, action, "copytree(action[2] + folder, action[1] + folder)", bcolors.OKGREEN)
