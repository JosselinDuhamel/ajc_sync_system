#!usr/bin/python3.9
# coding: utf-8

import os
from app.bcolor import bcolors

def sync():
    print("\n" + bcolors.OKGREEN + "Start sync\n" + bcolors.ENDC)
    return "Synchronizing"
