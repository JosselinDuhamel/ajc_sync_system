import sqlite3
from sqlite3 import Error

class MyConnect:
    dbName: str
    con: sqlite3.Connection
    schema: str
    cur: sqlite3.Cursor

    def __init__(self, name: str):
        self.state = False
        self.dbName = name

    def connectDB(self):
        try:
            self.con = sqlite3.connect(self.dbName, check_same_thread=False, timeout=5)
            self.cur = self.con.cursor()
        except Exception as e:
            print(e)

    def deconnectBD(self):
        self.con.close()

    def activate(self):
        try:
            self.cur.execute("CREATE TABLE IF NOT EXISTS save"\
                             +" (id integer PRIMARY KEY,"\
                             +"path text, "\
                             +"dest text, "\
                             +"type text, "\
                             +"taille text, "\
                             +"action text, "\
                             +"modif text)")
            self.con.commit()
        except Error as e:
            print(e)

    def insertVal(self, val: tuple):
        try:
            if len(val) == 6:
                sql = "INSERT INTO save(path,dest,type,taille,action,modif)" + \
                "VALUES(?,?,?,?,?,?)"
                self.cur.execute(sql, val)
                self.con.commit()
            else:
                print(f'requete {val}: manque des champs pour remplir la table')
        except Error as e:
            print(e)
            self.con.rollback()

    def insertVals(self, vals: list):
        try:
            for val in vals:
                if len(val) == 6:
                    sql = "INSERT INTO save(path,dest,type,taille,action,modif)" + \
                    "VALUES(?,?,?,?,?,?)"
                    self.cur.execute(sql, val)
                else:
                    print(f'requete {val}: manque des champs pour remplir la table')
            self.con.commit()
        except Error as e:
            print(e)
            self.con.rollback()

    def getContent(self):
        rows = ""
        try:
            self.cur.execute("SELECT * FROM save")
            self.con.commit()
            rows = cur.fetchall()
        except Error as e:
            print(e)
            self.con.rollback()
        return rows

    def deactivate(self):
        try:
            cur = self.con.cursor()
            self.cur.execute('''DELETE FROM save''')
            self.con.commit()
            self.cur.execute('''VACUUM''')
            self.con.commit()
        except Error as e:
            print(e)
            self.con.rollback()
