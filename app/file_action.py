#!usr/bin/python3.9
# coding: utf-8

import app
import os
import app.thread
from shutil import copyfile
from app.bcolor import bcolors
from app.db import save

def find_file(file, search_path):
    for dirpath, sub_dirs, filenames in os.walk(search_path):
        if file in filenames:
            return True
    return False

def get_extension(file, dir):
    path = (file).split('.')
    if len(path) > 1:
        return '.' + path[len(path) - 1]
    else:
        return None

def sync_file(file, action, command, color):
    try:
        extension = get_extension(file, action[1])
        if app.filter_flag != None:
            if extension == app.filter_flag:
                exec(command)
                print ("\n\n" +bcolors.OKBLUE + "File " + bcolors.ENDC + file + color,
                       action[0], bcolors.OKBLUE + "in both directories" +  bcolors.ENDC)
                return True
        else:
            exec(command)
            print ("\n\n" +bcolors.OKBLUE + "File " + bcolors.ENDC + file + color,
                   action[0], bcolors.OKBLUE + "in both directories" +  bcolors.ENDC)
            return True
    except OSError as e:
        print("Error: %s : %s" % (action[1] + file, e.strerror))
    return False

def file_action(file, action):
    if action[0] == "removed":
        return sync_file(file, action, "os.remove(action[1] + file)", bcolors.FAIL)
    if action[0] == "created":
        return sync_file(file, action, "copyfile(action[2] + file, action[1] + file)", bcolors.OKGREEN)
