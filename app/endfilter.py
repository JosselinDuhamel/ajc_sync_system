#!usr/bin/python3.9
# coding: utf-8

from app.bcolor import bcolors

def endfilter():
    print("\n" + bcolors.OKBLUE + "Filter unset" + bcolors.ENDC +"\n")
    return None
